<?php

use Maaaxim\MongoYoutube\Migration\FillDataMigration;

require_once "./vendor/autoload.php";

$migration = new FillDataMigration();
$migration->up();
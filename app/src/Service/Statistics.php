<?php

namespace Maaaxim\MongoYoutube\Service;

use MongoDB\Driver\Command;
use MongoDB\Driver\Manager;
use stdClass;

/**
 * Class Statistics
 * @package Maaaxim\MongoYoutube\Service
 */
class Statistics
{
    protected $manager;

    /**
     * Statistics constructor.
     */
    public function __construct()
    {
        $this->manager = new Manager('mongodb://mongo-db');
    }

    /**
     * Суммарное кол-во лайков и дизлайков для канала по всем его видео
     *
     * @param $channelId
     * @return mixed
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function getLikesDislikesInfo($channelId)
    {
        $command = new Command([
            'aggregate' => 'videos',
            'pipeline' => [
                [
                    '$match' => [
                        'channel' => 'htaywbqdfprobsmwlyvn'
                    ]
                ],
                [
                    '$lookup' => [
                        "from" => "channels",
                        "localField" => "channel",
                        "foreignField" => "id",
                        "as" => "channelDetails"
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$channel',
                        'likesTotal' => [
                            '$sum' => '$likeCount'
                        ],
                        'dislikeTotal' => [
                            '$sum' => '$dislikeCount'
                        ]
                    ]
                ],
                [
                    '$limit' => 1
                ]
            ],
            'cursor' => new stdClass
        ]);
        $cursor = $this->manager->executeCommand('youtube', $command);
        foreach($cursor as $item){
            return $item;
        }
    }

    /**
     * Топ N каналов с лучшим соотношением кол-во лайков/кол-во дизлайков
     *
     * @return array
     * @throws \MongoDB\Driver\Exception\Exception
     */
    public function getTopChannels()
    {
        $topChannels = [];

        // посчитаем лайк-рейтинг (лайки/дизлайки)
        $map = 'function () {
                if(this.dislikeCount == 0)
                    rating = this.likeCount
                else
                    rating = this.likeCount / this.dislikeCount;
                emit(this.channel, rating)
        }';

        // сгруппируем по рейтингу, посчитаем среднее по каналу
        $reduce = 'function (key, values) {
                return Array.avg(values)
        }';

        $command = new Command([
            'mapreduce' => 'videos',
            "map" => $map,
            'reduce' => $reduce,
            "out" => "top_channels"
        ]);

        $this->manager->executeCommand('youtube', $command);

        // отсортируем
        $options = [
            'sort' => ['value' => -1],
            'limit' => 10
        ];

        $query = new \MongoDB\Driver\Query([], $options);
        $cursor = $this->manager->executeQuery('youtube.top_channels', $query);

        foreach ($cursor as $document) {
            $topChannels[] = $document;
        }

        return $topChannels;
    }
}
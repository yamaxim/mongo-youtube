<?php

namespace Maaaxim\MongoYoutube\Migration;

use Faker\Factory;
use Maaaxim\MongoYoutube\Model\Channel;
use Maaaxim\MongoYoutube\Model\Video;
use MongoDB\BSON\UTCDateTime;

class FillDataMigration {

    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * @var array
     */
    protected $channels;

    /**
     * FillDataMigration constructor.
     */
    public function __construct()
    {
        $this->faker = Factory::create();
        $this->channels = [];
    }

    /**
     *
     */
    public function up()
    {
        $this->addChannels();
        $this->addVideos();
    }

    /**
     * add channels
     */
    protected function addChannels()
    {
        $channel = new Channel();
        for($i = 0; $i < 100; $i++){
            $channelId = $this->generateString(20);
            if(!in_array($channelId, $this->channels)){
                $this->channels[] = $channelId;
                $channel->add([
                    "id" => $channelId,
                    "title" => $this->faker->name
                ]);
            }
        }
    }

    /**
     * add videos
     */
    protected function addVideos()
    {
        $video = new Video();
        $channelCount = sizeof($this->channels);
        for($i = 0; $i < 100000; $i++){
            $video->add([
                "id" => $this->generateString(20),
                "channel" => $this->channels[rand(0, $channelCount-1)],
                "title" => $this->faker->name,
                "date" => new UTCDateTime(rand(1262055681, 1262055681)),
                "likeCount" => rand(0, 10000),
                "dislikeCount" => rand(0, 1000),
                "viewCount" => rand(0, 100000),
            ]);
        }
    }

    /**
     * Generates random string
     *
     * @param int $size
     * @return string
     */
    protected function generateString($size = 2)
    {
        $title = "";
        foreach(range(1, $size) as $t)
            $title .= chr(rand(97, 122));
        return $title;
    }
}
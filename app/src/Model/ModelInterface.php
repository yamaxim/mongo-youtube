<?php

namespace Maaaxim\MongoYoutube\Model;

interface ModelInterface
{
    /**
     * @param array $array
     */
    public function add(array $array): void;

    /**
     * @param array $array
     */
    public function remove(array $array): void;
}
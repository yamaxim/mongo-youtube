<?php

namespace Maaaxim\MongoYoutube\Model;

class Channel extends BaseModel
{
    const COLLECTION = 'youtube.channels';
}
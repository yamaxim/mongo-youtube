<?php

namespace Maaaxim\MongoYoutube\Model;

use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Exception\BulkWriteException;
use MongoDB\Driver\Manager;

/**
 * Class BaseModel
 * @package Maaaxim\MongoYoutube\Model
 */
class BaseModel implements ModelInterface
{
    protected $manager;

    /**
     * BaseModel constructor.
     */
    public function __construct()
    {
        $this->manager = new Manager('mongodb://mongo-db');
    }

    /**
     * @param array $array
     */
    public function add(array $array): void
    {
        $bulk = new BulkWrite;
        $bulk->insert($array);
        $this->manager->executeBulkWrite(static::COLLECTION, $bulk);
    }

    /**
     * @param array $array
     */
    public function remove(array $array): void
    {
        $bulk = new BulkWrite;
        $bulk->delete($array);
        $this->manager->executeBulkWrite(static::COLLECTION, $bulk);
    }
}
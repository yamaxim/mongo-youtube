connection = new Mongo();
db = connection.getDB("youtube");

var channelsJson = cat("/var/mongo/channels.json");
db.createCollection("channels", {
    validator: {
        $jsonSchema: JSON.parse(channelsJson)
    }
});

var videosJson = cat("/var/mongo/videos.json");
db.createCollection("videos", {
    validator: {
        $jsonSchema: JSON.parse(videosJson)
    }
});